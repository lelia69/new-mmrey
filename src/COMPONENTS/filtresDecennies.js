
import { data2 } from '../DATAS/data2'
import { getGeojsonPoints, getGeojson, destroyLayers, displayCorrespondants } from '../COMPONENTS/principalsFunctions'
import { map } from '../index'


export let datArray = data2.features;

let btn1 = document.querySelector('#btn1');
let btn2 = document.querySelector('#btn2');
let btn3 = document.querySelector('#btn3');
let btn4 = document.querySelector('#btn4');
let btn0 = document.querySelector('#all');







//////////////////////////////////////////////////////////////FILTRES DECENNIES//////////////////////////////////////////////////




// 1ère Décennie (lines)
let firstDecennie = datArray.filter(function (item) {
  return item.properties.date >= "1740-12-31" && item.properties.date <= "1750-12-31"
})

export let newFirstDecennie = getGeojson(firstDecennie)
export let newPointsFirst = getGeojsonPoints(firstDecennie)






// 2ème Décennie (lines)
let secondDecennie = datArray.filter(function (item) {
  return item.properties.date >= "1750-12-31" && item.properties.date <= "1760-12-31"
});

export let newSecondDecennie = getGeojson(secondDecennie)
export let newSecondPoints = getGeojsonPoints(secondDecennie)





// 3ème Décennie (lines)
let thirdDecennie = datArray.filter(function (item) {
  return item.properties.date >= "1760-12-31" && item.properties.date <= "1770-12-31"
})

export let newThirdDecennie = getGeojson(thirdDecennie)
export let newThirdPoints = getGeojsonPoints(thirdDecennie)





// 4ème Décennie (lines) 
let fourDecennie = datArray.filter(function (item) {
  return item.properties.date >= "1770-12-31" && item.properties.date <= "1780-12-31"
})

export let newFourDecennie = getGeojson(fourDecennie)
export let newFourPoints = getGeojsonPoints(fourDecennie)






///////////////////////////////////////////////////////////////////////////////////////////// GESTION BOUTONS DECENNIES //////////////////////////////////////////////////////////////////////////////////:

// 1741 - 1750

btn1.addEventListener('click', () => {


  destroyLayers()




  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'lettres1',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });




  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'points1',
    filter: ['has', 'point_count'],
    paint: {

      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'points1',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'points1',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  });


  // on trie les expediteurs et destinataires afin de ne pas avoir de doublons puis on les 
  // concatène en un seul même tableau


  const uniqueFirst = [...new Set(firstDecennie.map(item => item.properties.ark_expe))]
  const uniqueFirst2 = [...new Set(firstDecennie.map(item => item.properties.ark_dest))]

  const newUniqueFirst = uniqueFirst.concat(uniqueFirst2)




  displayCorrespondants(newUniqueFirst, '1741 à 1750')





});





// 1751 - 1760

btn2.addEventListener('click', () => {


  destroyLayers()



  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'lettres2',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });





  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'points2',
    filter: ['has', 'point_count'],
    paint: {
      // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
      // with three steps to implement three types of circles:
      //   * Blue, 20px circles when point count is less than 100
      //   * Yellow, 30px circles when point count is between 100 and 750
      //   * Pink, 40px circles when point count is greater than or equal to 750
      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'points2',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'points2',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  });


  const uniqueSecond = [...new Set(secondDecennie.map(item => item.properties.ark_expe))]
  const uniqueSecond2 = [...new Set(secondDecennie.map(item => item.properties.ark_dest))]

  const newUniqueSecond = uniqueSecond.concat(uniqueSecond2)




  displayCorrespondants(newUniqueSecond, '1751 à 1760');


})






// 1761 - 1770
btn3.addEventListener('click', () => {


  destroyLayers()



  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'lettres3',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });





  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'points3',
    filter: ['has', 'point_count'],
    paint: {
      // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
      // with three steps to implement three types of circles:
      //   * Blue, 20px circles when point count is less than 100
      //   * Yellow, 30px circles when point count is between 100 and 750
      //   * Pink, 40px circles when point count is greater than or equal to 750
      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'points3',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'points3',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  });



  const uniqueThird = [...new Set(thirdDecennie.map(item => item.properties.ark_expe))]
  const uniqueThird2 = [...new Set(thirdDecennie.map(item => item.properties.ark_dest))]

  const newUniqueThird = uniqueThird.concat(uniqueThird2)



  displayCorrespondants(newUniqueThird, '1761 - 1770')

})




// 1771 -1780
btn4.addEventListener('click', () => {


  destroyLayers()




  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'lettres4',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });





  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'points4',
    filter: ['has', 'point_count'],
    paint: {
      // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
      // with three steps to implement three types of circles:
      //   * Blue, 20px circles when point count is less than 100
      //   * Yellow, 30px circles when point count is between 100 and 750
      //   * Pink, 40px circles when point count is greater than or equal to 750
      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'points4',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'points4',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  });



  const uniqueFour = [...new Set(fourDecennie.map(item => item.properties.ark_expe))]
  const uniqueFour2 = [...new Set(fourDecennie.map(item => item.properties.ark_dest))]

  const newUniqueFour = uniqueFour.concat(uniqueFour2)


  displayCorrespondants(newUniqueFour, '1771 - 1780');

})





btn0.addEventListener('click', () => {

  let reload = () => {
    window.location.reload()
  }
  reload()


});