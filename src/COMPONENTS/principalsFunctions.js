import { gens } from '../DATAS/gens'
import { data2 } from '../DATAS/data2'
import { map } from '../index';


let DATAS = data2.features

// fonction qui permet de concaténer l'en-tete necessaire à transformer notre tab en objet Geojson et de le parser pour qu'il puisse être compatible avec MAPBOX
export function getGeojson(value) {
  let newData = `{
      "type": "FeatureCollection",
      "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
      "features": ` + JSON.stringify(value) + `}`
  return JSON.parse(newData)
}





export function getGeojsonPoints(value) {

  const newData = `{
  "type": "FeatureCollection",
  "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
  "features": ` + JSON.stringify((displayPoints(value))) + `}`

  return JSON.parse(newData)
}





// function qui permet de renseigner les clusters en fonction des lines
export function displayPoints(value) {
  let newData = []

  for (let i = 0; i < value.length; i++) {
    newData.push(
      {
        "type": "Feature", "properties":
        {
          "id": value[i].properties.ville_expedition,
          "date": value[i].properties.date,
          "libraire_expediteur": value[i].properties.libraire_expediteur,
          "libraire_destinataire": value[i].properties.libraire_destinataire,
          "auteur_expediteur": value[i].properties.auteur_expediteur,
          "auteur_destinataire": value[i].properties.auteur_destinataire

        }, "geometry": {
          "type": "Point",
          "coordinates": value[i].geometry.coordinates[0]
        }
      })

    newData.push(
      {
        "type": "Feature", "properties":
        {
          "id": value[i].properties.ville_reception,
          "date": value[i].properties.date,
          "libraire_expediteur": value[i].properties.libraire_expediteur,
          "libraire_destinataire": value[i].properties.libraire_destinataire,
          "auteur_expediteur": value[i].properties.auteur_expediteur,
          "auteur_destinataire": value[i].properties.auteur_destinataire

        }, "geometry": {
          "type": "Point",
          "coordinates": value[i].geometry.coordinates[1]
        }
      })
  }

  return newData
}




// Affichage des correspondants en fonction des décennies choisies

export function displayCorrespondants(tab, title) {

  const aside = document.querySelector('#aside')
  aside.innerHTML = ' '

  const ul = document.createElement('ul')
  ul.innerHTML = ' '
  ul.className = "asideul"

  const titre = document.createElement('h3')
  titre.innerHTML = ' '
  titre.style.color = '#002458'
  titre.style.fontWeight = 'bolder'
  titre.style.textAlign = 'center'
  titre.textContent = title


  aside.appendChild(titre)
  aside.appendChild(ul)

  for (const datas of tab) {

    const li = document.createElement('li')
    li.id = datas

    const key = 'https://ark.ihrim.fr/' + datas.toString()

    let noms = (!datas ?  'ark manquant' : gens[key]['http://www.w3.org/2004/02/skos/core#prefLabel'][0].value)

    let ark = datas.toString()
    li.textContent = noms
    li.className = "liCorresp"
    li.addEventListener("click", () => {
      displayOneCorrespondant(ark,key, noms)
      displayMapByCorrespondants(ark, map)


    })
    ul.appendChild(li)


  }
};




// fonction qui affiche un correspondant + sa note + bouton redirigeant vers son index plus précis
 export function displayOneCorrespondant(ark, key, noms) {


  const aside = document.querySelector('#aside')
  aside.innerHTML = ' '

  const titre = document.createElement('h3')
  const p = document.createElement('p')


  titre.textContent = noms
  p.textContent =  (gens[key]['http://www.w3.org/2004/02/skos/core#note']? gens[key]['http://www.w3.org/2004/02/skos/core#note'][0].value :"Pas de note disponible") // permet de récupérer la note en lien avec la correspondant
  
  
  
  
  const href = document.createElement('a')
  href.href = 'http://corpus.ihrim.huma-num.fr/index/personnes/occurrences/' + ark + '/' + noms // envoie sur l'index principal via l'ark et le nom du correspondant cliqué
  href.textContent = "En savoir plus"


  aside.appendChild(titre)
  aside.appendChild(p)
  aside.appendChild(href)




}





// Suppression map 

export function destroyLayers() {
  if (map.getLayer('lettres')) map.removeLayer('lettres');
  if (map.getLayer('clusters')) map.removeLayer('clusters');
  if (map.getLayer('cluster-count')) map.removeLayer('cluster-count');
  if (map.getLayer('unclustered-point')) map.removeLayer('unclustered-point');
  if (map.getSource('unique')) map.removeSource('unique')
  if (map.getSource('uniquepoints')) map.removeSource('uniquepoints')
}





// a voir si on peut dynamiser pour clés villes + dates 
// fonction qui permet d'afficher les échanges d'un correspondant sur la map
export function displayMapByCorrespondants(value, map) {
  
  let tab = DATAS.filter(function (corresp) {
    return corresp.properties.ark_dest == value || corresp.properties.ark_expe == value
  });
  const essai = getGeojson(tab)
  const essai2 = getGeojsonPoints(tab)

  destroyLayers()

  map.addSource('unique', {
    'type': 'geojson',
    'data': essai

  });


  map.addSource('uniquepoints', {
    'type': 'geojson',
    'data': essai2,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });

  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'unique',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });

  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'uniquepoints',
    filter: ['has', 'point_count'],
    paint: {

      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'uniquepoints',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'uniquepoints',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  })


}



export function displayMap(tab, map) {
  
  
  const essai = getGeojson(tab)
  const essai2 = getGeojsonPoints(tab)

  destroyLayers()

  map.addSource('unique', {
    'type': 'geojson',
    'data': essai

  });


  map.addSource('uniquepoints', {
    'type': 'geojson',
    'data': essai2,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });

  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'unique',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });

  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'uniquepoints',
    filter: ['has', 'point_count'],
    paint: {

      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'uniquepoints',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'uniquepoints',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  })


}