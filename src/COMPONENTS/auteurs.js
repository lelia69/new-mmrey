import { datArray } from "./filtresDecennies";
import {getGeojson,getGeojsonPoints, destroyLayers,displayCorrespondants} from './principalsFunctions'
import {map} from '../index'
// AUTEURS

let auteurBtn = document.querySelector('#auteurBtn');



let resultAuteur = datArray.filter(function (auteur) {
    return auteur.properties.auteur_destinataire == 1
  });
  let resultAuteur2 = datArray.filter(function (auteur) {
    return auteur.properties.auteur_expediteur == 1
  });
  
  const allAuteurs = resultAuteur.concat(resultAuteur2)
  
  
  const uniqueAuteur = [...new Set(resultAuteur.map(item => item.properties.ark_dest))]
  
  
  const uniqueAuteur2 = [...new Set(resultAuteur2.map(item => item.properties.ark_expe))]
  
  
  let newUniqueAuteur = uniqueAuteur.concat(uniqueAuteur2);

 export let newAuteur = getGeojson(allAuteurs)
 export let newAuteurs = getGeojsonPoints(allAuteurs)
  
  

  
  /////////////////////////////////// GESTION BOUTON AUTEURS //////////////////////////////////////////////////////
  
  
  auteurBtn.addEventListener('click', () => {
  
    destroyLayers()
    displayCorrespondants(newUniqueAuteur, 'Les Auteurs')
   
  
    map.addLayer({
      'id': 'lettres',
      'type': 'line',
      'source': 'auteurs',
      'layout': {
        'line-join': 'round',
        'line-cap': 'round'
      },
      'paint': {
        'line-color': '#002458',
        'line-width': 1
      }
    });
  
    map.addLayer({
      id: 'clusters',
      type: 'circle',
      source: 'auteurspoints',
      filter: ['has', 'point_count'],
      paint: {
  
        'circle-color': [
          'step',
          ['get', 'point_count'],
          '#51bbd6',
          100,
          '#f1f075',
          750,
          '#f28cb1'
        ],
        'circle-radius': [
          'step',
          ['get', 'point_count'],
          20,
          100,
          30,
          750,
          40
        ]
      }
    });
  
    map.addLayer({
      id: 'cluster-count',
      type: 'symbol',
      source: 'auteurspoints',
      filter: ['has', 'point_count'],
      layout: {
        'text-field': '{point_count_abbreviated}',
        'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
        'text-size': 12
      }
    });
  
    map.addLayer({
      id: 'unclustered-point',
      type: 'circle',
      source: 'auteurspoints',
      filter: ['!', ['has', 'point_count']],
      paint: {
        'circle-color': '#11b4da',
        'circle-radius': 4,
        'circle-stroke-width': 1,
        'circle-stroke-color': '#fff'
      }
    });
  
  
  
  
  
  })
  