
import {datArray} from './filtresDecennies'
import {getGeojson,getGeojsonPoints, destroyLayers,displayCorrespondants} from './principalsFunctions'
import {map} from '../index'


let libraireBtn = document.querySelector('#libraireBtn');






// LIBRAIRES

// récupérer tous les objets où "libraire" == true en deux tableaux distincts ( un expé, un destinataire)
let resultLibraire = datArray.filter(function (libraire) {
    return libraire.properties.libraire_expediteur == 1
  });
  let resultLibraire2 = datArray.filter(function (libraire) {
    return libraire.properties.libraire_destinataire == 1
  });

  
  // permet de récupérer un tableau de tous les échanges libraires
  const allLibraires = resultLibraire.concat(resultLibraire2)
 
  
  // récupérer les noms des libraires expediteurs et destinataires en deux tableaux également ( un expé, un destinataire)
  const uniqueLib = [...new Set(resultLibraire.map(item => item.properties.ark_expe))] // permet de trier les noms d'expediteurs
  const uniqueLib2 = [...new Set(resultLibraire2.map(item => item.properties.ark_dest))] // permet de trier les noms de destinataires
  
  
  // Concatène mes expediteurs et mes destinataires qui sont libraires
  let newUniqueLib = uniqueLib.concat(uniqueLib2);
  
  // transforme le tab de libraire en objet geojson lisible par MAPBOX
  export let newLibraire = getGeojson(allLibraires)
  
  // transforme le tab de libraire en objet geojson mais cette fois ci avec le type "point" pour initialiser les clusters
  export let newLibrairePoints = getGeojsonPoints(allLibraires)
  
  
  
  /////////////////////////////////////////////// GESTION BOUTONS LIBAIRES//////////////////////////////////////////
  libraireBtn.addEventListener('click', () => {
  
    destroyLayers()
    displayCorrespondants(newUniqueLib, 'Les Libraires')
  
    map.addLayer({
      'id': 'lettres',
      'type': 'line',
      'source': 'libraires',
      'layout': {
        'line-join': 'round',
        'line-cap': 'round'
      },
      'paint': {
        'line-color': '#002458',
        'line-width': 1
      }
    });
  
    map.addLayer({
      id: 'clusters',
      type: 'circle',
      source: 'librairespoints',
      filter: ['has', 'point_count'],
      paint: {
  
        'circle-color': [
          'step',
          ['get', 'point_count'],
          '#51bbd6',
          100,
          '#f1f075',
          750,
          '#f28cb1'
        ],
        'circle-radius': [
          'step',
          ['get', 'point_count'],
          20,
          100,
          30,
          750,
          40
        ]
      }
    });
  
    map.addLayer({
      id: 'cluster-count',
      type: 'symbol',
      source: 'librairespoints',
      filter: ['has', 'point_count'],
      layout: {
        'text-field': '{point_count_abbreviated}',
        'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
        'text-size': 12
      }
    });
  
    map.addLayer({
      id: 'unclustered-point',
      type: 'circle',
      source: 'librairespoints',
      filter: ['!', ['has', 'point_count']],
      paint: {
        'circle-color': '#11b4da',
        'circle-radius': 4,
        'circle-stroke-width': 1,
        'circle-stroke-color': '#fff'
      }
    });
  
  
  
  });