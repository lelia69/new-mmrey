
import { newFirstDecennie, newSecondDecennie, newThirdDecennie, newFourDecennie, newPointsFirst, newSecondPoints, newThirdPoints, newFourPoints } from '../src/COMPONENTS/filtresDecennies'
import { newLibraire, newLibrairePoints } from '../src/COMPONENTS/libraires'
import { destroyLayers, getGeojson, getGeojsonPoints, displayCorrespondants, displayMap } from '../src/COMPONENTS/principalsFunctions'

import { newAuteur, newAuteurs } from './COMPONENTS/auteurs'
import List from 'list.js'

import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import { data2 } from './DATAS/data2';
import _, { uniq } from 'underscore'; // librairie 



/////////////////////////////////////// INITIALISATION MAP //////////////////////////////////////////////


// affichage MAP 
mapboxgl.accessToken = 'pk.eyJ1IjoibGVsaWE2OSIsImEiOiJja3ZoMXdpaTc0NHNqMm5sdXR6eXplNnRuIn0.wxtzskLia-ykb1S333sEvw'
export let map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mingarao/ckvgkogoh64dq18rsda4eor5m',
  center: [4.8958, 52.3739],
  zoom: 5

});

map.on('load', () => {


  destroyLayers()

  // sources lines
  map.addSource('lettres0', {
    'type': 'geojson',
    'data': getGeojson(data2.features)

  });

  map.addSource('lettres1', {
    'type': 'geojson',
    'data': newFirstDecennie

  });

  map.addSource('lettres2', {
    'type': 'geojson',
    'data': newSecondDecennie

  });

  map.addSource('lettres3', {
    'type': 'geojson',
    'data': newThirdDecennie

  });

  map.addSource('lettres4', {
    'type': 'geojson',
    'data': newFourDecennie

  });

  map.addSource('libraires', {
    'type': 'geojson',
    'data': newLibraire

  });

  map.addSource('auteurs', {
    'type': 'geojson',
    'data': newAuteur

  });




  // sources clusters
  map.addSource('points', {
    'type': 'geojson',
    'data': getGeojsonPoints(data2.features),
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });


  map.addSource('points1', {
    'type': 'geojson',
    'data': newPointsFirst,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });

  map.addSource('points2', {
    'type': 'geojson',
    'data': newSecondPoints,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });


  map.addSource('points3', {
    'type': 'geojson',
    'data': newThirdPoints,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });

  map.addSource('points4', {
    'type': 'geojson',
    'data': newFourPoints,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });

  map.addSource('librairespoints', {
    'type': 'geojson',
    'data': newLibrairePoints,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });





  map.addSource('auteurspoints', {
    'type': 'geojson',
    'data': newAuteurs,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

  });






  // affichage lines
  map.addLayer({
    'id': 'lettres',
    'type': 'line',
    'source': 'lettres0',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#002458',
      'line-width': 1
    }
  });






  // affichage clusters

  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'points',
    filter: ['has', 'point_count'],
    paint: {

      'circle-color': [
        'step',
        ['get', 'point_count'],
        '#51bbd6',
        100,
        '#f1f075',
        750,
        '#f28cb1'
      ],
      'circle-radius': [
        'step',
        ['get', 'point_count'],
        20,
        100,
        30,
        750,
        40
      ]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'points',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12
    }
  });

  map.addLayer({
    id: 'unclustered-point',
    type: 'circle',
    source: 'points',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': '#11b4da',
      'circle-radius': 4,
      'circle-stroke-width': 1,
      'circle-stroke-color': '#fff'
    }
  });

  const firstAside = document.querySelector("#aside")
  firstAside.innerHTML = ' '
  const titre = document.createElement('h3')
  titre.textContent = 'Marc Michel Rey'
  titre.style.color = '#002458'
  titre.style.fontWeight = 'bolder'
  titre.style.textAlign = 'center'
  titre.style.marginTop = '3%'

  const text = document.createElement('p')
  text.style.color = '#002458'
  text.style.fontSize = '16px'
  text.style.width = "100%"
  text.style.textAlign = 'center'
  text.textContent = `

  Au plus fort de sa carrière, le « réseau » de Marc Michel Rey libraire s’étend de la Russie à l’est à l’Angleterre à l’ouest ; de Copenhague au nord à Avignon au sud. Rey entretient également des relations commerciales avec le Suriname où s’est installé son fils Isaac (Rio Demerary).
  Les correspondants de Rey sont des auteurs dont le libraire édite le manuscrit, des libraires avec lesquels il entretient des relations commerciales, des hommes de lettres ou des savants qui envoient des comptes rendus d’ouvrages ou des lettres pour les journaux, s’enquièrent des nouveautés de la librairie ou proposent des manuscrits à publier. D’autres encore, académiciens, militaires, aristocrates, professeurs sont des clients.
  
  La carte permet d’accéder aux correspondants à partir de la ville d’expédition des lettres, Marc Michel Rey étant situé à Amsterdam. Chaque correspondant dispose d’une fiche d’identité et la carte permet de visualiser le volume des lettres échangées avec Rey. Le volume indiqué désigne le volume total de la correspondance connue à ce jour (lettres reçues et lettres envoyées).
  Les onglets permettent de visualiser les échanges épistolaires par période, par type de correspondant (libraire, auteur) et par personnes (voir portraits).
  `
  firstAside.appendChild(titre)
  firstAside.appendChild(text)


});


















///////////////////////////////////////////////////// SEARCHBAR //////////////////////////////////////////////////


let DATAS = data2.features
let input = document.getElementById('research')
let option1 = document.querySelector('#noms')
let option2 = document.querySelector('#dates')
let option3 = document.querySelector('#lieux')
let ul = document.getElementById("values")

let dataNom = []



// on récupère uniquement les Noms + ark 
DATAS.forEach(function (data) {
  dataNom.push(
    { clé1: data.properties.destinataire, clé2: data.properties.ark_dest },
    { clé1: data.properties.expediteur, clé2: data.properties.ark_expe }
  )

})


let dataDate = []

// on récupère uniquement les Dates
DATAS.forEach(function (data) {
  dataDate.push(
    { clé1: data.properties.date.substring(0, 4) }
  )

})

let dataLieux = []

// on récupère uniquement les Lieux + ark 
DATAS.forEach(function (data) {
  dataLieux.push(
    { clé1: data.properties.ville_expedition, clé2: data.properties.ark_ville_expe },
    { clé1: data.properties.ville_reception, clé2: data.properties.ark_ville_dest }
  )
})



// Liste des tableaux par catégories
let newUniqueNom = _.uniq(dataNom, 'clé1') // on supprime les noms en doublon

let newUniqueDate = _.uniq(dataDate, 'clé1')

let newUniqueLieux = _.uniq(dataLieux, 'clé1')


// Permet de créer les Li via la libraire list.js
let options = {
  valueNames: ['clé1', 'clé2'],
  item: '<li><span   class="clé1"></span><span class="clé2" style="opacity: 0"></span></li>'
};


function changeTabNom(tab) {
  let userList = new List('recherche', options, tab)
  let li = document.querySelectorAll('#values li')
  li.forEach(function (item) {
    item.addEventListener('click', () => {

      let span = item.querySelector('.clé2').innerHTML
      let span2 = item.querySelector('.clé1').innerHTML

      let arkDatas = DATAS.filter(function (data) {
        let datas = data.properties.ark_expe == span
        return (!datas ? data.properties.ark_dest == span : datas)
      })
     
      ul.style.display = 'none'
      input.value = span2

      const uniqueArkExpe = [...new Set(arkDatas.map(item => item.properties.ark_expe))]
      const uniqueArkDest = [...new Set(arkDatas.map(item => item.properties.ark_dest))]



      const newUniqueArk = uniqueArkDest.concat(uniqueArkExpe)


      displayCorrespondants(newUniqueArk, span2)
      displayMap(arkDatas, map)



    })

  })
}



function changeTabDate(tab) {
  let userList = new List('recherche', options, tab)
  let li = document.querySelectorAll('#values li')
  li.forEach(function (item) {
    item.addEventListener('click', () => {
      let span = item.querySelector('.clé1').innerHTML

      let arkDatas = DATAS.filter(function (data) {
        return data.properties.date.substring(0, 4) == span
      })

      ul.style.display = 'none'
      input.value = span

      const uniqueArkExpe = [...new Set(arkDatas.map(item => item.properties.ark_expe))]
      const uniqueArkDest = [...new Set(arkDatas.map(item => item.properties.ark_dest))]

      const newUniqueArk = uniqueArkDest.concat(uniqueArkExpe)
      displayCorrespondants(newUniqueArk, span)
      displayMap(arkDatas, map)

    })

  })
}


function changeTabLieux(tab) {
  let userList = new List('recherche', options, tab)
  let li = document.querySelectorAll('#values li')
  li.forEach(function (item) {
    item.addEventListener('click', () => {


      let span = item.querySelector('.clé2').innerHTML
      let span2 = item.querySelector('.clé1').innerHTML

      ul.style.display = 'none'
      input.value = span2


      let arkDatas = DATAS.filter(function (data) {
        let datas = data.properties.ark_ville_dest == span
        let datas2 = data.properties.ark_ville_expe == span
        return datas + datas2
      })
      console.log(arkDatas, span);

      const uniqueArkExpe = [...new Set(arkDatas.map(item => item.properties.ark_expe))]
      const uniqueArkDest = [...new Set(arkDatas.map(item => item.properties.ark_dest))]

      const newUniqueArk = uniqueArkExpe.concat(uniqueArkDest)

      displayCorrespondants(newUniqueArk, span2)
      displayMap(arkDatas, map)
    })

  })
}







// Pour chaque button, la searchbar load un nouveau tableau de catégories
option1.addEventListener('click', () => {
  ul.innerHTML = ' '
  input.value = ' '
  changeTabNom(newUniqueNom)

});


option2.addEventListener('click', () => {
  ul.innerHTML = ' '
  input.value = ' '
  changeTabDate(newUniqueDate)

});


option3.addEventListener('click', () => {
  ul.innerHTML = '  '
  input.value = ' '
  changeTabLieux(newUniqueLieux)


})











/////// STYLE UL ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
input.addEventListener('keydown', () => {
  let ul = document.getElementById('values')
  ul.style.display = 'block'
})

/*input.addEventListener('click', () => {
  let ul = document.getElementById('values')
  ul.style.display = 'none'
})*/












