export const lieux = {
    "https://ark.ihrim.fr/ark:/43094/rey0g9q808x6t" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6289",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6289",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.9225",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.47917",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2747891",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Rotterdam",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Rotterdam",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Rotterdam",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Rotterdam",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Rotterdam",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Роттердам",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey0kvbjr2rrs" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6553",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2921044",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9x220hq4x0",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyb5fszvh8fr",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Deutschland",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Germany",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Allemagne",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Germania",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Duitsland",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Германия",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey165jd7h0rc" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171003",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171003",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.215933",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "19.134422",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/798544",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9ng8jnksfv",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyxr47hsgmcm",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Polen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Poland",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Pologne",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Polonia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Polen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Польша",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey177dm63n0h" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6230",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6230",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.85045",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.34878",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2800866",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Brüssel",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Brussels",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Bruxelles",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Bruxelles",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Brussel",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Брюссель",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey1jv5zs2gr5" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6976##6288",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6288",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "5.7947829",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-58.408436",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reywmpgcx12jn",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Rio Demerary",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey1txvp0m23k" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6263##171019",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171019",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.4864249",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-0.1285899",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2636063",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#note" : [
        {
          "value" : "Fleuve qui traverse Londres <br>",
          "type" : "literal",
          "lang" : "fr"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "The Thames",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Tamise (la)",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey2k4vh7z74k" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6223",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6223",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.21989",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.40346",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2803138",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Antwerpen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Antwerp",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Anvers",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Anversa",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Antwerpen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Антверпен",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey2vt81npp3w" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6807##6293",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6293",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "59.93863",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "30.31413",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyv5qphfgrvt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/498817",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Sankt Petersburg",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "St Petersburg",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Saint-Pétersbourg",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "San Pietroburgo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Sint-Petersburg",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Санкт-Петербург",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey376ghx9dk8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6253",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6253",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.6763",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "7.4809",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#altLabel" : [
        {
          "value" : "Haselune",
          "type" : "literal",
          "lang" : "fr"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Haselünne",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey3gh0156m8b" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6255",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6255",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Klein Lankum",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6659",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey0g9q808x6t",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey2k4vh7z74k",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3gh0156m8b",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3x98568vq4",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey6r4392bp69",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey7nj4zb97sw",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfd7bdgkghb",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reygbdc7vndc3",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyhdtgt47x9g",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyjn42c6ddrr",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reym9081rzhvd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyn93kb3gxzp",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyrzdp990ks5",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reysf118w92bx",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reysx3nk2d1h1",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reytx4cdsvrzz",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvph1zs3mrh",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyw495rzzpnq",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reywkr3x993kx",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyxxzshs82xn",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyzg0vsw93hn",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyznbtjkd216",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Pays-Bas (Provinces-Unies)",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey3x98568vq4" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6237",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6237",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.00667",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.35556",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2757345",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Delft",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Delft",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Delft",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Delft",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Delft",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Делфт",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey439714q6fs" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "324154",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324154",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.75",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/countries/NL/netherlands.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Hollande",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey4411z00pz1" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171011",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171011",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "56.8406494",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "24.7537645",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/458258",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyjbc2grtph3",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Lettland",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Latvia",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Lettonie",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Lettonia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Letland",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Латвия",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey477cx12k14" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6304",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6304",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.46299",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.84345",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2658145",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Vevey",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Vevey",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Vevey",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Vevey",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Vevey",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Веве",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey4mmf1zqsgx" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6298",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6298",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.2854",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "1.82129",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2971692",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Trie-Château",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey4wzbnvgj2x" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##324144",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324144",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.66667",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "1.11407",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/11609039/leicestershire.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Leicestershire",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey57ckmpbfk8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6660##6261",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6261",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "38.71667",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-9.13333",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9dmkb0xtcj",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2267057",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Lissabon",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Lisbon",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Lisbonne",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Lisbona",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Lissabon",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Лиссабон",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey59d682dggp" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6242",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6242",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.12648",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.69772",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3019906",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Ermenonville",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Эрменонвиль",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6864",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey477cx12k14",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5cvnt8v1gn",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5pgf7wgbf5",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5qx5s46xq4",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9bd839zcx4",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfv439ts33x",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfzckgz9m4f",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyj3w45sdpj6",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reym7tsjkrk21",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reynk220jn2xd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reytb6n7gc9gj",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvf8955889w",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Suisse (République de Genève)",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5cvnt8v1gn" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6275",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6275",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.91108",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.61111",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2659572",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Môtiers",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5dtmbc4xc8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-21",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##324118",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324118",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-21",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.68439",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.18496",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/2990999/nancy.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Nancy",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5nc2z0h7vd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6277",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6277",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.96236",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "7.62571",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2867543",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Münster / Nordrhein-Westf.",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Münster",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Münster",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Münster",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Münster",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Мюнстер",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5pgf7wgbf5" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6247",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6247",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.26642",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.15406",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/7285904",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Genthod",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Жанто",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5qx5s46xq4" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6308",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6308",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.77852",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.64115",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2657941",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Yverdon-les-Bains",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Ивердон-ле-Бен",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey5tpc7n5vt0" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6559##6560##6232",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6232",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "45.56465",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.92645",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reykz3drs38nf",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3027422",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Chambéry",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Chambéry",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Chambéry",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Chambéry",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Chambéry",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Шамбери",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey60v8k76fjg" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6252",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6252",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.13423",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "8.91418",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2911007",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Hanau",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Hanau",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Hanau",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Hanau",
          "type" : "literal",
          "lang" : "nl"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey6g2dsnqf9h" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6234",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6234",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.49271",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-0.25801",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2653121",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Chiswick",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Чизик",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey6r4392bp69" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6310",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6310",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.5125",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.09444",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2743477",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Zwolle",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Zwolle",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Zwolle",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Zwolle",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Zwolle",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Зволле",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey6r804gx1jf" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6251",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6251",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "53.57532",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "10.01534",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2911298",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Hamburg",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Hamburg",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Hambourg",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Amburgo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Hamburg",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Гамбург",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey7nj4zb97sw" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6222",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6222",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-07-03",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.37403",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.88969",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2759794",
          "type" : "uri"
        },
        {
          "value" : "http://www.wikidata.org/entity/Q727",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Amsterdam",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Amsterdam",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Amsterdam",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Amsterdam",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Amsterdam",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Амстердам",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey7s8g56kh7x" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##170993##170995",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "170995",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "36.5297438",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-6.2928976",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9w5mpvkpj2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2520600",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Cadiz",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Cadiz",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Cadix",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Cadiz",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Cadiz",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Кадис",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey81m91r254m" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##170997",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "170997",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.1106444",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "8.6820917",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2925533",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Frankfurt am Main",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Frankfurt",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Francfort-sur-le-Main",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Francoforte sul Meno",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Frankfurt am Main",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Франкфурт-на-Майне",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey8n5rshtz45" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6299",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6299",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.30073",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.08524",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2971549",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Troyes",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Troyes",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Troyes",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Troyes",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Troyes",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Труа",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey8pq1wtc523" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6267",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6267",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.4891",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "8.46694",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2873891",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Mannheim",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Mannheim",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Mannheim",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Mannheim",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Mannheim",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Мангейм",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey9bd839zcx4" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6285",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6285",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.82192",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.93817",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2659243",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Payerne",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey9dmkb0xtcj" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6660",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6660",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "39.6945",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-8.13057",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2264397",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey57ckmpbfk8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Portugal",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Portugal",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Portugal",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Portogallo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Portugal",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Португалия",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey9ng8jnksfv" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171003##171009",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171009",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "54.361434",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "18.6282185",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey165jd7h0rc",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3099434",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Danzig",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Gdansk",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Gdańsk",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Danzica",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Gdansk",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Гданьск",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey9w5mpvkpj2" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##170993",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "170993",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "39.3260685",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-4.8379791",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2510769",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey7s8g56kh7x",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Spanien",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Spain",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Espagne",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Spagna",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Spanje",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Испания",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/rey9x220hq4x0" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6554",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6554",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey0kvbjr2rrs",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reywr20g0npjj",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reywz38kdvtc2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Prusse",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyb5fszvh8fr" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-21",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##324119",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324119",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-21",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.33962",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "12.37129",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey0kvbjr2rrs",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/2879139/leipzig.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Leipzig",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reybrhzhvt9tk" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6807##6274",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6274",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "55.75222",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "37.61556",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyv5qphfgrvt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/524901",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Moskau",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Moscow",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Moscou",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Mosca",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Moskou",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Москва",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyc2ghrxt3rj" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6276",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6276",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.13743",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "11.57549",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2867714",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "München",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Munich",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Munich",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Monaco di Baviera",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "München",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Мюнхен",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6558",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.0",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.0",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3017382",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey4mmf1zqsgx",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey59d682dggp",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5dtmbc4xc8",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey8n5rshtz45",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfk6g1f3dk2",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfqpmgmqzpd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfwd0z21znm",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyg14c217bg8",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reygfxzs5bz1p",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyh990mx2hgh",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyhr22d6s27z",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyjcmxwn63hf",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyjqc9qz41n1",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyjxvw2c5tq0",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reykn550whzmz",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reykzwqfg0fz3",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reymj07xgkrkz",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reymsjsc745j0",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyngm2skkwh7",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyp20rtv5sxt",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyr151nqmhdf",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyr3v53h216q",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvf1sczwn06",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvmk83dqnj1",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyxzn99th06v",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyzh2j5j7qdq",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Frankreich",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "France",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "France",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Francia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Frankrijk",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Франция",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reycvscqpwmpd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6263##171015",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171015",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.5116502",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-0.1371919",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#note" : [
        {
          "value" : "Place de Londres.",
          "type" : "literal",
          "lang" : "fr"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Golden Square",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Golden Square",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6263",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6263",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.50853",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-0.12574",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2643743",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey1txvp0m23k",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycvscqpwmpd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyq8wk2tfjj8",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyv2hhcpqgpf",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyx4bm0z0njv",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "London",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "London",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Londres",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Londra",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Londen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Лондон",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfd7bdgkghb" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6262",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6262",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.1175",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.01944",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2751524",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Loenen",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfgcq3hpsbw" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6559##6593",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6593",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyhh70hdb1j6",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvnv2m20dgk",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Trentin",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfk6g1f3dk2" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6225",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6225",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "43.94834",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.80892",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3035681",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Avignon",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Avignon",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Avignon",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Avignone",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Avignon",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Авиньон",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfn74ns6fgg" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6976##6283",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6283",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "5.86638",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-55.16682",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reywmpgcx12jn",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3383330",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Paramaribo",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Paramaribo",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Paramaribo",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Paramaribo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Paramaribo",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Парамарибо",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfqpmgmqzpd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6271",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6271",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Monquin",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfv439ts33x" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6246",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6246",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.20222",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.14569",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2660646",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Genf",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Geneva",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Genève",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Ginevra",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Genève",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Женева",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfwd0z21znm" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6239",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6239",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.09501",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.75614",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3002680",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Le Plessis Belleville",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyfzckgz9m4f" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6278",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6278",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.99179",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.931",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2659496",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Neuenburg",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Neuchâtel",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Невшатель",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyg14c217bg8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6303",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6303",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.80359",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.13424",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2969679",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Versailles",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Versailles",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Versailles",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Versailles",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Versailles",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Версаль",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reygbdc7vndc3" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6221",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6221",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.76564",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.94592",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/12022128",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Altena",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reygfxzs5bz1p" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6286",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6286",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.90702",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.05635",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2986306",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Pont-à-Mousson",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Pont-à-Mousson",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Pont-à-Mousson",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Pont-à-Mousson",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Pont-à-Mousson",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Понт-а-Муссон",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6963",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey0kvbjr2rrs",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey165jd7h0rc",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey177dm63n0h",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey4411z00pz1",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9dmkb0xtcj",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9w5mpvkpj2",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyhh70hdb1j6",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksg62xbsfq",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyn0s4sz6dwg",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyqz790kpd0n",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyr55scxw1rq",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyv4rq3b3q28",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyv5qphfgrvt",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyws4n8r7b86",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Europe",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reygsf2v8m748" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6557##6235",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6235",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "55.67594",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "12.56553",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyws4n8r7b86",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2618425",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Kopenhagen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Copenhagen",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Copenhague",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Copenaghen",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Kopenhagen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Копенгаген",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reygxmwm703cd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6241",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6241",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Elberfeld",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyh990mx2hgh" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6266",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6266",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.29566",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.40935",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2996444",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Malesherbes",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyhdtgt47x9g" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6248",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6248",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.75902",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.73882",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2755358",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Grave",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Grave",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Grave",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Grave",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Grave",
          "type" : "literal",
          "lang" : "nl"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyhh70hdb1j6" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6559",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6559",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfgcq3hpsbw",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reykz3drs38nf",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Italie",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyhr22d6s27z" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6272",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6272",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.98826",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.3434",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2992229",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Montmorency",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Монморанси",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyhwftkf4ght" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6305",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6305",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.83652",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "10.78216",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2810808",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Wernigerode",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Wernigerode",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Wernigerode",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Wernigerode",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Вернигероде",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyj3w45sdpj6" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6227",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6227",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.94809",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "7.44744",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2661552",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Bern",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Bern",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Berne",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Berna",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Bern",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Берн",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyjbc2grtph3" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171011##171013",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171013",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "56.9493977",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "24.1051846",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey4411z00pz1",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/456172",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Riga",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Riga",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Riga",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Riga",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Riga",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Рига",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyjcmxwn63hf" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6296",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6296",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.58392",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "7.74553",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2973783",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Straßburg",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Strasbourg",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Strasbourg",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Strasburgo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Straatsburg",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Страсбург",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyjk42z1v683" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "324156",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324156",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.7520209",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-1.2577263",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/2640729/oxford.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Oxford",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyjn42c6ddrr" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6300",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6300",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.09083",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.12222",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2745912",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Utrecht",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Utrecht",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Utrecht",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Utrecht",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Utrecht",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Утрехт",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyjqc9qz41n1" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6273",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6273",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "43.61092",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "3.87723",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2992166",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Montpellier",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Montpellier",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Montpellier",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Montpellier",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Montpellier",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Монпелье",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyjxvw2c5tq0" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6284",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6284",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.85341",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.3488",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2988507",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Paris",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Paris",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Paris",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Parigi",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Parijs",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Париж",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reykn550whzmz" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6243",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6243",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.25858",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.11063",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3018679",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Ferney-Voltaire",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Ferney-Voltaire",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Ferney",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Ferney-Voltaire",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Ferney-Voltaire",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Ферне-Вольтер",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reykptmjccrxr" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171005##171007",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171007",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "59.3251172",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "18.0710935",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyn0s4sz6dwg",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2673730",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Stockholm",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Stockholm",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Stockholm",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Stoccolma",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Stockholm",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Стокгольм",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyksg62xbsfq" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6228",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6228",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.79324",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.06703",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2801283",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Bouillon",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Bouillon",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Bouillon",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Bouillon",
          "type" : "literal",
          "lang" : "nl"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6555",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey0kvbjr2rrs",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey376ghx9dk8",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5nc2z0h7vd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey60v8k76fjg",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey6r804gx1jf",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey81m91r254m",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey8pq1wtc523",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyc2ghrxt3rj",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reygxmwm703cd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyhwftkf4ght",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyp6vc14bwqd",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyqm4rvtv850",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reytq51pfc2m9",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyxnpcr4mk7g",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyxr8jw3m9hb",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "SERG",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reykz3drs38nf" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6559##6560",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6560",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyhh70hdb1j6",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5tpc7n5vt0",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reywsx52fgz5x",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Savoie (états sardes)",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reykzwqfg0fz3" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##324145",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324145",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "41.98859",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "8.99827",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/3023517/corse.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Corse",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reym7tsjkrk21" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6279",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6279",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "47.12545",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "7.24033",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2659474",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Nidau",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Нидау",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reym9081rzhvd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6244",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6244",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.4425",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "3.57361",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2745392",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Vlissingen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Flushing",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Flessingue",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Flessinga",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Vlissingen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Влиссинген",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reymj07xgkrkz" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6281",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6281",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "47.90289",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "1.90389",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2989317",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Orléans",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Orléans",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Orléans",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Orléans",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Orléans",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Орлеан",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reymsjsc745j0" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6301",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6301",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "48.8079589",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.2410047",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Val Fleury",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyn0s4sz6dwg" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171005",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171005",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "59.6749712",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "14.5208584",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2661886",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reykptmjccrxr",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Schweden",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Sweden",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Suède",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Svezia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Zweden",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Швеция",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyn93kb3gxzp" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6224",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6224",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.98",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.91111",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2759661",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Arnheim",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Arnhem",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Arnheim",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Arnhem",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Arnhem",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Арнем",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyngm2skkwh7" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6231",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6231",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "44.05",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.05",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3028542",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Carpentras",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Carpentras",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Carpentras",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Carpentras",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Carpentras",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Карпантрас",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reynk220jn2xd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6254",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6254",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Île Saint-Pierre",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyp20rtv5sxt" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6290",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6290",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.44313",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "1.09932",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2982652",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Rouen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Rouen",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Rouen",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Rouen",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Rouen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Руан",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyp6vc14bwqd" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6295",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6295",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.67416",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "9.56102",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2838459",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Schlitz",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Шлиц",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyq8wk2tfjj8" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6263##171017",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171017",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.512221",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-0.1341851",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#note" : [
        {
          "value" : "Apparait sous la forme  Brower Street  dans les lettres du chevalier d'Eon (corpus MMR).",
          "type" : "literal",
          "lang" : "fr"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Brewer Street",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Brewer Street",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyqm4rvtv850" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6311",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6311",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.77664",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.08342",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3247449",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Aachen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Aachen",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Aix-la-Chapelle",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Aquisgrana",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Aken",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Ахен",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyqz790kpd0n" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6280",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6280",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Nienhuys",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyr151nqmhdf" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6264",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6264",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "45.74846",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.84671",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2996944",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Lyon",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Lyon",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Lyon",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Lione",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Lyon",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Лион",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyr3v53h216q" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6287",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6287",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.03333",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2984114",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Reims",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Реймс",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyr55scxw1rq" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6292",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6292",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.02668",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.37401",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2787407",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Saint-Hubert",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyrq29n0s2kh" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6307",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6307",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.269463",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-1.773391",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Wootton Hall",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyrzdp990ks5" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6258",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6258",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.15833",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.49306",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2751773",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Leiden",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Leiden",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Leyde",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Leida",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Leiden",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Лейден",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reysf118w92bx" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6245",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6245",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "53.18546",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.54123",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2755845",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Franeker",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Franeker",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Франекер",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reysvdtrrg5jq" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6259",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6259",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.68154",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-1.82549",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2644531",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Lichfield",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Lichfield",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Lichfield",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Lichfield",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Личфилд",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reysx3nk2d1h1" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6302",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6302",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.37",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.16806",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2745641",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Venlo",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Venlo",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Venlo",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Venlo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Venlo",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Венло",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reytb6n7gc9gj" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6233",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6233",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.9599",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.77498",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/11962760",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Champ-du-Moulin",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reytq51pfc2m9" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6236",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6236",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.87167",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "8.65027",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2938913",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Darmstadt",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Darmstadt",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Darmstadt",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Darmstadt",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Darmstadt",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Дармштадт",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reytx4cdsvrzz" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6265",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6265",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.78917",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.55556",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2751299",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Maasdam",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyv2hhcpqgpf" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6263##324143",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "324143",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-10-25",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.51575",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "0.15571",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "https://www.geonames.org/12041974/portman-square.html",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Portman Square",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyv4rq3b3q28" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6260",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6260",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "50.63373",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "5.56749",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2792413",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Lüttich",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Liège",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Liège",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Liegi",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Luik",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Льеж",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyv5qphfgrvt" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6807",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6807",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "60.0",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "100.0",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2017370",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey2vt81npp3w",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reybrhzhvt9tk",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Russland",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Russia",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Russie",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Russia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Rusland",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Россия",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyvf1sczwn06" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6269",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6269",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "43.43054",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "0.30794",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2993947",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Mielan",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Мьелан",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyvf8955889w" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6864##6257",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6257",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.516",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.63282",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey5bg713hw1g",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2659994",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Lausanne",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Lausanne",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Lausanne",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Losanna",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Lausanne",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Лозанна",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyvmk83dqnj1" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6268",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6268",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "49.11911",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.17269",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2994160",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Metz",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Metz",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Metz",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Metz",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Metz",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Мец",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyvnv2m20dgk" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6559##6593##6297",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6297",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "46.34298",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "11.04539",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfgcq3hpsbw",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3165904",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Tassullo",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Tassullo",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Tassullo",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Tassullo",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Тассулло",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyvph1zs3mrh" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6256",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6256",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-07-03",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.07667",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.29861",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2747373",
          "type" : "uri"
        },
        {
          "value" : "http://www.wikidata.org/entity/Q36600",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Den Haag",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "The Hague",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "La Haye",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "L'Aia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Den Haag",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Гаага",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyvx7wx8ff8v" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6556",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey4wzbnvgj2x",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey6g2dsnqf9h",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyrq29n0s2kh",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reysvdtrrg5jq",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Angleterre",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyw495rzzpnq" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6249",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6249",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.38084",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.63683",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2755003",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Haarlem",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Haarlem",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Haarlem",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Haarlem",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Haarlem",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Харлем",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reywkr3x993kx" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6309",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6309",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.13833",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.20139",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2743608",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Zutphen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Zutphen",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Zutphen",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Zutphen",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Zutphen",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Зютфен",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reywmpgcx12jn" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6976",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6976",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey1jv5zs2gr5",
          "type" : "uri"
        },
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfn74ns6fgg",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Amériques",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reywr20g0npjj" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6554##6294",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6294",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "54.52156",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "9.5586",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9x220hq4x0",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2838634",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Schleswig",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Шлезвиг",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyws4n8r7b86" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6557",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6557",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "56.0",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "10.0",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyggrvvbpmt8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2623032",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#narrower" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reygsf2v8m748",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Dänemark",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Denmark",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Danemark",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Danimarca",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Denemarken",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Дания",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reywsx52fgz5x" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6559##6560##6270",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6270",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "45.46427",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "9.18951",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reykz3drs38nf",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3173435",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Mailand",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Milan",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Milan",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Milano",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Milaan",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Милан",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reywz38kdvtc2" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6554##6226",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6226",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.52437",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "13.41053",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey9x220hq4x0",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2950159",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Berlin",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Berlin",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Berlin",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Berlino",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Berlijn",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Берлин",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyx4bm0z0njv" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6556##6263##170999",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "170999",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.5097269",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "-0.1231649",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyfbhsrfxw33",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#note" : [
        {
          "value" : "The Strand est une rue importante de Londres",
          "type" : "literal",
          "lang" : "fr"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "The Strand",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "The Strand",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyxnpcr4mk7g" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6240",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6240",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.22172",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.77616",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2934246",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Düsseldorf",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Düsseldorf",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Düsseldorf",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Düsseldorf",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Düsseldorf",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Дюссельдорф",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyxr47hsgmcm" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##171003##171001",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "171001",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2021-05-07",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.2319581",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "21.0067249",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey165jd7h0rc",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/6695624",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Warschau",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Warsaw",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Varsovie",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Varsavia",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Warschau",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "варшава",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyxr8jw3m9hb" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6553##6555##6229",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6229",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reyksv80mrqjt",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Brunswick",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyxxzshs82xn" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6306",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6306",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.48294",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "4.63816",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2744732",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Westerhout",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyxzn99th06v" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6282",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6282",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "47.78802",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "7.50777",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2989084",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Ottmarsheim",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Ottmarsheim",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Ottmarsheim",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Ottmarsheim",
          "type" : "literal",
          "lang" : "nl"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyzg0vsw93hn" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6291",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6291",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Rusthoff",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyzh2j5j7qdq" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6558##6238",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6238",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "51.03297",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "2.377",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/reycdqxr312c2",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/3020686",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Dünkirchen",
          "type" : "literal",
          "lang" : "de"
        },
        {
          "value" : "Dunkirk",
          "type" : "literal",
          "lang" : "en"
        },
        {
          "value" : "Dunkerque",
          "type" : "literal",
          "lang" : "fr"
        },
        {
          "value" : "Dunkerque",
          "type" : "literal",
          "lang" : "it"
        },
        {
          "value" : "Duinkerke",
          "type" : "literal",
          "lang" : "nl"
        },
        {
          "value" : "Дюнкерк",
          "type" : "literal",
          "lang" : "ru"
        }
      ]
    },
    "https://ark.ihrim.fr/ark:/43094/reyznbtjkd216" : {
      "http://purl.org/dc/terms/created" : [
        {
          "value" : "2019-05-27",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/dc/terms/description" : [
        {
          "value" : "6963##6659##6250",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/identifier" : [
        {
          "value" : "6250",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#string"
        }
      ],
      "http://purl.org/dc/terms/modified" : [
        {
          "value" : "2019-05-28",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#date"
        }
      ],
      "http://purl.org/umu/uneskos#memberOf" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/?idg=G178&idt=69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" : [
        {
          "value" : "http://www.w3.org/2004/02/skos/core#Concept",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#lat" : [
        {
          "value" : "52.1",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2003/01/geo/wgs84_pos#long" : [
        {
          "value" : "6.27694",
          "type" : "literal",
          "datatype" : "http://www.w3.org/2001/XMLSchema#double"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#broader" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/43094/rey3k16t3qkm8",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#exactMatch" : [
        {
          "value" : "http://www.geonames.org/2754976",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#inScheme" : [
        {
          "value" : "https://ark.ihrim.fr/ark:/69",
          "type" : "uri"
        }
      ],
      "http://www.w3.org/2004/02/skos/core#prefLabel" : [
        {
          "value" : "Hackfort",
          "type" : "literal",
          "lang" : "fr"
        }
      ]
    }
  }